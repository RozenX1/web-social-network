class Images < ActiveRecord::Migration
    def up
      add_attachment :users, :avatar
      add_attachment :entries, :image
    end

    def down
      remove_attachment :users, :avatar
      remove_attachment :entries, :image
    end
end
