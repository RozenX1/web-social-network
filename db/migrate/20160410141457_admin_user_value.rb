class AdminUserValue < ActiveRecord::Migration
  def change
    add_column :users, :admin, :boolean, default: false
    add_column :users, :ban, :boolean, default: false
  end
end
