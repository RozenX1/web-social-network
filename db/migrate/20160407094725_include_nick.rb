class IncludeNick < ActiveRecord::Migration
  def change
    add_column :users, :nick, :string, unique:true
  end
end
