json.array!(@users) do |user|
  json.extract! user, :id, :name, :email, :description, :birthday, :followees_count, :followers_count, :likees_count, :mentioners_count, :created_at, :updated_at, :admin, :nick, :ban
  json.entries_count user.entries.length
end
