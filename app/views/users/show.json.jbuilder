json.extract! @user, :id, :name, :nick, :email, :birthday, :description, :birthday, :followees_count, :followers_count, :likees_count, :mentioners_count, :created_at, :updated_at, :admin, :nick, :ban
json.avatar_url @user.avatar.url
json.entries_count @user.entries.length
json.following @user.followed_by? current_user
