angular.module "fakebook-app"

.controller "adminUsersCtrl", ["$scope", "userResource", "$http", "paths", "headers",
($scope, userResource, $http, paths, headers) ->
  userResource.query (users) ->
    $scope.users = users

  $scope.ban = (user) ->
    url = paths.api.users.toggle_ban.replace ":id", user.id
    # PUT /api/users/:id/toggle_ban
    $http.put url, {}, headers: headers
    .then ->
      user.ban = !user.ban

  $scope.make_admin = (user) ->
    url = paths.api.users.make_admin.replace ":id", user.id
    # PUT /api/users/:id/make_admin
    $http.put url, {}, headers: headers
    .then ->
      user.admin = yes
]

.controller "adminEntriesCtrl", ["$scope", "entryResource", "alertsManager", "$routeParams", "$route",
($scope, entryResource, alertsManager, $routeParams, $route) ->

  entryResource.query {id_user: $routeParams.id}, (entries) ->
    $scope.entries = entries

  $scope.delete = (entry)->
    params =
      id: entry.id
      id_user: entry.user_id

    entryResource.delete params, ->
      alertsManager.add "info", "Entry was removed!"
      $route.reload()

]
