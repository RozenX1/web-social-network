angular.module "fakebook-app"
.factory "userResource", ["$resource", "paths", "headers", ($resource, paths, headers) ->
  $resource paths.api.users.index+"/:id", {id:"@id"}, {
    update:
      method: 'PUT'
    get:
      headers: headers
    query:
      headers: headers
      isArray: yes
  }
]
