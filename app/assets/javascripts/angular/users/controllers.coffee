angular.module "fakebook-app"

.controller "userShowCtrl", ["$scope", "userResource", "$routeParams", "paths", "entryResource", "$http", "headers", ($scope, userResource, $routeParams, paths, entryResource, $http, headers) ->
  # GET /api/users/:id
  userResource.get {id: $routeParams.id}, (user) ->
    $scope.user = user
    $scope.is_current_user = if $scope.user.id is $scope.current_user.id then yes else no
    # GET /api/users/:id/entries
    entryResource.query {id_user: $scope.user.id}, (entries) ->
      $scope.entries = entries

    if not $scope.is_current_user
      $scope.following = user.following

  $scope.toggle_follow = ->
    url = paths.api.users.toggle
    .replace(":id", $scope.current_user.id)
    .replace(":id_followable", $scope.user.id)
    # PUT /api/users/:id/toggle/:id_followable
    $http.put(url, headers:headers)
    .then ->
      $scope.following = not $scope.following
      if $scope.following
        $scope.user.followers_count++
      else
        $scope.user.followers_count--
]

.controller "userNickRedirectCtrl", ["paths", "$routeParams", "$http", "headers", "$location"
(paths, $routeParams, $http, headers, $location) ->
  url = paths.api.users.by_nick.replace ":nick", $routeParams.nick
  # GET /api/users/nick/:nick
  $http.get(url, headers:headers)
  .then (resp) ->
    url = paths.users.index+"/"+resp.data.id
    $location.path url
  , (resp) ->
    $location.path paths.not_found
]

.controller "userEditCtrl", ["$scope", "Upload", "paths", "$location", "alertsManager", "userStorage", "userResource",
($scope, Upload, paths, $location, alertsManager, userStorage, userResource) ->
  $scope.editing = no
  $scope.new_description = $scope.current_user.description
  $scope.new_name = $scope.current_user.name

  $scope.toggle_edit = ->
    $scope.editing = not $scope.editing

  $scope.save = ->
    data =
      new_description: $scope.new_description
      new_name: $scope.new_name
    data.avatar = $scope.avatar if $scope.avatar?

    url = paths.api.users.index+"/"+$scope.current_user.id

    Upload.upload({
      url: url,
      data: data,
      method: "PUT"
    }).then (resp) ->
      userResource.get {id: $scope.user.id}, (user) ->
        alertsManager.add "info", "Profile updated successfully!"
        $scope.user = user
        if user.id is $scope.current_user.id
          userStorage.user user
        $scope.toggle_edit()
]
