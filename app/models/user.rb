class User < ActiveRecord::Base
  acts_as_followable
  acts_as_liker
  acts_as_follower
  acts_as_mentionable

  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :entries
  validates :nick, uniqueness: true

  has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>" },
    default_url:  ActionController::Base.helpers.asset_path('default-user.jpg')
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/

  def active_for_authentication?
    super && !self.ban
  end
end
