class UsersController < ApplicationController
  before_action :set_user, only: [:show, :update, :destroy, :toggle_follow, :toggle_ban, :make_admin]
  before_action :set_followable, only: [:toggle_follow]
  before_action :check_owner, only: [:update, :destroy]
  before_action :check_admin, only: [:toggle_ban, :make_admin]
  respond_to :json

  # GET /users
  def index
    @users = User.all
    respond_with @users
  end

  # GET /users/term/:term
  def index_by_term
    @users = User.where('nick LIKE ?', "%#{params[:term]}%")
    @users = @users.limit(params[:limit]) if params[:limit]
    respond_with @users
  end

  # GET /users/:id
  def show
    respond_with @user
  end

  # GET /users/nick/:nick
  def show_by_nick
    @user = User.where(nick: params[:nick]).first
    if @user.nil?
      render json: {"error":"not-found"}.to_json, status: :not_found
    else
      respond_with @user
    end
  end

  # PUT /users/:id
  def update
    @user.name = params[:new_name]
    @user.description = params[:new_description]
    @user.avatar = params[:avatar] if params[:avatar]
    @user.save
    render nothing:true
  end

  # PUT /users/:id/toggle/:id_followable
  def toggle_follow
    @user.toggle_follow! @followable
    render nothing:true
  end

  # PUT /users/:id/toggle_ban
  def toggle_ban
    @user.ban = !@user.ban
    @user.save
    render nothing:true
  end

  # PUT /users/:id/admin
  def make_admin
    @user.admin = true
    @user.save
    render nothing:true
  end

  private
    def check_admin
      unless current_user.admin
        render nothing:true, status: :forbidden
      end
    end

    # NOTE: current_user -/> @user. They can be different
    def check_owner
      if current_user.id != @user.id
        render nothing:true, status: :forbidden
      end
    end

    def find id
      if User.exists? id
        User.find id
      else
        render nothing:true, status: :not_found
      end
    end

    def set_user
      id = (params[:id] or params[:user_id])
      @user = find id
    end

    def set_followable
      @followable = find params[:id_followable]
    end

end
