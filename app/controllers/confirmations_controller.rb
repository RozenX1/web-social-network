class ConfirmationsController < Devise::ConfirmationsController
  clear_respond_to
  respond_to :json

  # GET /resource/confirmation?confirmation_token=abcdef
  # We need to reimplement this for confirmation redirection to root
  def show
    self.resource = resource_class.confirm_by_token(params[:confirmation_token])
    set_flash_message :notice, :confirmed
    redirect_to root_path
  end

  private

    def after_confirmation_path_for(resource_name, resource)
      root_path
    end

    def after_resending_confirmation_instructions_path_for(resource_name)
      root_path
    end
end
