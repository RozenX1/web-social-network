class RegistrationsController < Devise::RegistrationsController
  clear_respond_to
  respond_to :json

  private

  def sign_up_params
    params.require(:user).permit(:name, :email, :nick, :birthday, :password, :password_confirmation)
  end

end
