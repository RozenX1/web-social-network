class EntriesController < ApplicationController
  before_action :set_user
  before_filter :set_entry, only: [:show, :edit, :update, :destroy, :like]
  before_filter :set_liker, only: [:like]
  before_filter :check_owner, only: [:create, :destroy]
  before_filter :check_chain, only: [:show, :like, :destroy]
  respond_to :json

  # GET /user/:user_id/entries
  def index
    @entries = if params[:feed]
      people = @user.followees(User) << @user
      people.inject([]){|list, elem| list + elem.entries}
    else
      @user.entries + @user.likees(Entry)
    end
    @entries.sort_by!{|x| - x.created_at.to_i}
    respond_with @user, @entries
  end

  # GET /user/:user_id/entries/:id
  def show
    respond_with @user, @entry
  end

  # POST /user/:user_id/entries
  def create
    @entry = Entry.new(entry_params)
    @entry.user = @user
    if @entry.save
      render nothing:true, status: :created
    else
      render json: @entry.errors, status: :unprocessable_entity
    end
  end

  # PUT /user/:user_id/entries/:id/like/:id_liker
  def like
    @liker.toggle_like! @entry
    render nothing:true
  end

  # DELETE /user/:user_id/entries/:id
  def destroy
    @entry.destroy
    render nothing:true, status: :ok
  end

  private
    def find_user id
      if User.exists? id
        User.find id
      else
        render nothing:true, status: :not_found
      end
    end

    def find_entry id
      if Entry.exists? id
        Entry.find id
      else
        render nothing:true, status: :not_found
      end
    end

    def set_user
      @user = find_user params[:user_id]
    end

    def set_entry
      id = (params[:id] or params[:entry_id])
      @entry = find_entry id
    end

    def set_liker
      @liker = find_user params[:id_liker]
    end

    def entry_params
      params.require(:entry).permit(:message, :title, :image)
    end

    def check_owner
      if current_user.id != @user.id
        render nothing:true, status: :forbidden
      end
    end

    def check_chain
      unless @user.entries.member? @entry
        render nothing:true, status: :not_found
      end
    end
end
